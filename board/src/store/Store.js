
import {applyMiddleware, combineReducers, compose } from "redux";
import ReduxThunk from 'redux-thunk'
import cardReducer from "./reducer";

const rootReducer = combineReducers({
  card: cardReducer,
});

const componentEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose
 const store = (rootReducer, {}, componentEnhancers(applyMiddleware(ReduxThunk)))

export default store;

