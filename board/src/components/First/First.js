import React from "react";

import "./TeamOne.scss";

function First() {
  const data = useSelector((state) => state.main?.mainData);

  const teamOneTotal = data?.home?.points;
  const teamTwoTotal = data?.away?.points;

  return (
    <div className="teamone">
      <div className="teamName">
        <h1 id="teamh1">
          {data?.home?.market} {data?.home?.name}
        </h1>
        
      </div>
      <h1 id="teamPoints">{teamOneTotal}</h1>
    </div>
  );
}

export default First;
