import React from "react";

import "/Secon.css"

function Second() {
  const data = useSelector((state) => state.main.mainData);

  const teamOneTotal = data?.home?.points;
  const teamTwoTotal = data?.away?.points;

  return (
    <div className="teamtwo">
      
      <div className="teamName">
        {teamTwoTotal > teamOneTotal }
        <h1 id="teamh1">
          {data?.away?.market} {data?.away?.name}
        </h1>
      </div>
      <h1 id="teamPoints">{teamTwoTotal}</h1>
    </div>
  );
}

export default TeamTwo;
