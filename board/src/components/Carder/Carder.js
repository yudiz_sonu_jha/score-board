import React from "react";
import Table from "../Table/Table";
import  First from "../First/First";
import  Second from "../Second/Second";

import "./CardContent.scss";

function Carder() {
  return (
    <div className="cardContent">
      <First/>
      <Table />
      <Second/>
    </div>
  );
}

export default Carder;
