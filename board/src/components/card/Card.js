import React from "react";

import "./Card.scss";
import CardContent from "../CardContent/CardContent";

function Card() {
  
  return (
    <div className="card">
      <h2>
        {data?.venue?.name}, {data?.venue?.city} | {date}
      </h2>
      <CardContent />
    </div>
  );
}

export default Card;
